import math
class Solution:
    def isPowerOfTwo(self, n: int) -> bool:
        return n > 0 and not round(math.log(n, 2), 14) % 1