from random import randint
class RandomizedSet:

    def __init__(self):
        self._data = {}
        self._keys = {}
        self.size = 0

    def __repr__(self):
        return f'{{{list(self._data.values()).__repr__().strip("[]")}}}'

    def insert(self, val: int) -> bool:
        if val in self._keys:
            return False
        self._data[self.size] = val
        self._keys[val] = self.size
        self.size += 1
        return True

    def remove(self, val: int) -> bool:
        if val not in self._keys:
            return False
        self.size -= 1
        key = self._keys[val]
        del self._keys[val]
        del self._data[key]
        try:
            repl = self._data[self.size]
            del self._data[self.size]
            self._data[key] = repl
            self._keys[repl] = key
        except KeyError:
            pass
        return True

    def getRandom(self) -> int:
        idx = randint(0, self.size - 1)
        return self._data[idx]
