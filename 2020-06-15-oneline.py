class Solution:
    def searchBST(self, root: TreeNode, val: int) -> TreeNode:
        return root if root is None or root.val == val else self.searchBST((root.left, root.right)[val > root.val], val)