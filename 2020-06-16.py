import re
class Solution:
    def validIPAddress(self, IP: str) -> str:
        ipv4 = re.fullmatch(r'((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]|[0-9])\.){3}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]|[0-9])', IP)
        if ipv4 is not None:
            return 'IPv4'
        ipv6 = re.fullmatch(r'([0-9a-fA-F]{1,4}:){7}[0-9a-fA-F]{1,4}', IP)
        if ipv6 is not None:
            return 'IPv6'
        return "Neither"
