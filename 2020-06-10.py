class Solution:
    def searchInsert(self, nums: List[int], target: int) -> int:
        ln = len(nums)
        mid = ln // 2
        if target < nums[0]:
            return 0
        if target > nums[-1]:
            return ln
        if target == nums[mid]:
            return mid
        return self.searchInsert(nums[:mid], target) + self.searchInsert(nums[mid:], target)