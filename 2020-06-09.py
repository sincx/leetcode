import re
class Solution:
    def isSubsequence(self, s: str, t: str) -> bool:
        return re.search(r'.*'.join(s), t) is not None