class Node:
    def __init__(self, value, next=None):
        self.value = value
        self.next = next

    def __repr__(self):
        return str(self.value)

class LL:
    def __init__(self, head):
        self.head = head
        curr = self.head
        self.size = 1
        while curr.next is not None:
            curr = curr.next
            self.size += 1
        self.tail = curr

    def __repr__(self):
        curr = self.head
        lst = []
        while curr is not None:
            lst.append(curr.value)
            curr = curr.next
        return lst.__repr__()

    def colorsort(self):
        curr = self.head
        prev = None
        i = 0
        while i < self.size:
            if curr.value == 0 and prev is not None:
                tmp = curr.next
                prev.next = tmp
                curr.next = self.head
                self.head = curr
                curr = tmp
            elif curr.value == 2 and curr.next is not None:
                tmp = curr.next
                if prev:
                    prev.next = tmp
                else:
                    self.head = tmp
                self.tail.next = curr
                curr.next = None
                self.tail = curr
                curr = tmp
            else:
                prev = curr
                curr = curr.next
            i += 1

def list_to_LL(lst):
    head = Node(lst[0])
    curr = head
    for item in lst[1:]:
        curr.next = Node(item)
        curr = curr.next
    return LL(head)

my_ll = list_to_LL([0, 2, 2, 1, 2, 1, 0, 2])
my_ll.colorsort()
print(my_ll)

my_ll = list_to_LL([1, 0])
my_ll.colorsort()
print(my_ll)

my_ll = list_to_LL([1, 2, 1, 2, 1, 2, 1])
my_ll.colorsort()
print(my_ll)

my_ll = list_to_LL([2, 2, 2, 0, 0, 0, 1, 2, 1, 0, 2, 1, 0, 0, 1, 2, 1, 1, 2, 1, 0, 0, 0, 0, 0, 0])
my_ll.colorsort()
print(my_ll)

my_ll = list_to_LL([0])
my_ll.colorsort()
print(my_ll)

my_ll = list_to_LL([2, 1, 0])
my_ll.colorsort()
print(my_ll)

my_ll = list_to_LL([0, 1, 2, 0, 1, 2])
my_ll.colorsort()
print(my_ll)